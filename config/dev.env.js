'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  APP_ROOT: '"http://localhost:8080/"',
  APP_ROOT_API: '"https://popup-990d9.firebaseio.com/settings/"',
  POPUP_ROOT_URI: '"http://localhost:8081/dist/static/"'
})
