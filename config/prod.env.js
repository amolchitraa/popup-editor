'use strict'
module.exports = {
  NODE_ENV: '"production"',
  APP_ROOT: '"http://localhost:8080/"',
  APP_ROOT_API: '"https://popup-990d9.firebaseio.com/settings/"',
  POPUP_ROOT_URI: '"http://67.20.105.42/vue-popup/popup/static/"'
}
