import axios from 'axios';
axios.defaults.baseURL = process.env.APP_ROOT_API;

export default {
	
    getSettings() {
    	return axios.get("options.json").then(response =>{
    		return response;
    	});
    	
    },
    saveSettings(data) {
    	console.log("data",data);
    	return axios.put("options.json", data).then(response =>{
    		return response;
    	});
    	
    },
    resetSettings() {
    	return axios.get("options_reset.json").then(response =>{
    		return response;
    	});    	
    },
}