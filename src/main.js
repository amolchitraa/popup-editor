// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import vdrag from 'v-drag';
import axios from 'axios';
import './assets/css/style.css';
import './assets/css/icomoon.css';

Vue.config.productionTip = false;
Vue.use(vdrag);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  vdrag,
  components: { App },
  template: '<App/>'
})
